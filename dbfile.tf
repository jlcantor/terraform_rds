# dbapp
resource "aws_db_instance" "default" {
  allocated_storage    = 10
  storage_type         = "gp2"
  engine               = "mysql"
  instance_class       = "db.t2.micro"
  name                 = "mydb_jl"
  username             = "Admin"
  password             = "Pa55w0rd"
  skip_final_snapshot  = true
}
