terraform{
	backend "s3"{
		bucket = "ea-terraform-training-lab"
		key = "jcantor_project/terraform.tfstate"
		region = "us-west-1"
	}
}
