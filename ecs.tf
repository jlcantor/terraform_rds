# cluster
resource "aws_ecs_cluster" "ea-training-cluster" {
    name = "ea-training-cluster"
}
resource "aws_launch_configuration" "ea-ecs-launchconfig" {
  name_prefix          = "ea-ecs-launchconfig"
  image_id             = "ami-4a2c192a"
  instance_type        = "t2.micro"
  key_name             = "EA-Lab-KeyPair"
  iam_instance_profile = "ecs-ec2-role"
  security_groups      = ["${aws_security_group.ecs-securitygroup.id}"]
  user_data            = "#!/bin/bash\necho 'ECS_CLUSTER=ea-training-cluster' > /etc/ecs/ecs.config\nstart ecs"
  lifecycle              { create_before_destroy = true }
}
resource "aws_autoscaling_group" "ea-ecs-autoscaling" {
  name                 = "ea-ecs-autoscaling"
  vpc_zone_identifier  = ["subnet-ad080ef5", "subnet-cd5dabaa"]
  launch_configuration = "${aws_launch_configuration.ea-ecs-launchconfig.name}"
  min_size             = 1
  max_size             = 1
  tag {
      key = "Name"
      value = "ea-ecs-ec2-container-jlcantor"
      propagate_at_launch = true
  }
}


